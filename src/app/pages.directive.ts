import { Directive, HostListener, Input } from '@angular/core';
import { ServiceNameService } from './shared/services/teammateState';

@Directive({
  selector: '[appPages]',
})
export class PagesDirective {
  constructor(private serviceNameService: ServiceNameService) {}
  @HostListener('click')
  countClick() {
    this.serviceNameService.increaseCounter();
  }
}
