import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ServiceNameService } from './shared/services/teammateState';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'cobiro';
  teammateCounter$: Observable<number>;
  constructor(private serviceNameService: ServiceNameService) {
    const data = [
      {
        price: 10,
      },
      {
        price: 20,
      },
    ];
    const total = data.reduce((prev, cur) => {
      return prev + cur.price;
    }, 0);
    this.teammateCounter$ = this.serviceNameService.teammateCounter$;
  }
}
