import { InjectionToken } from '@angular/core';

export const APP_URL = new InjectionToken<string>('APP_URL');
