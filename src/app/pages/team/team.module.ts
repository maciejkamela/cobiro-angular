import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material';
import { PagesDirective } from 'src/app/pages.directive';
import { HttpUserService } from 'src/app/shared/services/httpTeam.service';
import { TeamListComponent } from './team-list/team-list.component';

@NgModule({
  imports: [CommonModule, MatCardModule],
  declarations: [TeamListComponent, PagesDirective],
  exports: [TeamListComponent],
  providers: [
    {
      provide: 'GetTeam',
      useClass: HttpUserService,
    },
  ],
})
export class TeamModule {}
