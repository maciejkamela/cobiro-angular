import { ServiceNameService } from './../../../shared/services/teammateState';
import { Component, Inject, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Team } from 'src/app/shared/models/team.model';
import { GetTeam } from 'src/app/shared/models/team-service.model';

export interface Response {
  attributes: {
    title: string;
    memberCards: { [key: string]: string };
  };
}

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss'],
})
export class TeamListComponent implements OnInit {
  team$: Observable<Team>;
  teammateCounter$: Observable<number>;

  constructor(
    @Inject('GetTeam') private getTeam: GetTeam,
    private serviceNameService: ServiceNameService
  ) {}

  ngOnInit(): void {
    this.team$ = this.getTeamData();
    this.teammateCounter$ = this.serviceNameService.teammateCounter$;
  }

  getTeamData(): Observable<Team> {
    return this.getTeam.getTeamData();
  }

  trackByIndex(index): number {
    return index;
  }

  countClick(card: any) {
    this.serviceNameService.increaseCounter();
  }
}
