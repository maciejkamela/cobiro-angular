import { Team } from 'src/app/shared/models/team.model';
import { GetTeam } from '../../../shared/models/team-service.model';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

import { TeamListComponent } from './team-list.component';
import { DebugElement } from '@angular/core';
function getFirstNativeElement(parent: DebugElement[], children: string) {
  return parent[0].query(By.css(children)).nativeElement;
}

describe('TeamListComponent', () => {
  let component: TeamListComponent;
  let fixture: ComponentFixture<TeamListComponent>;
  let teamService: GetTeam;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatCardModule],
      declarations: [TeamListComponent],
      providers: [
        {
          provide: 'GetTeam',
          useValue: {
            getTeamData: () => of(),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamListComponent);
    component = fixture.componentInstance;
    teamService = TestBed.get('GetTeam');
    fixture.detectChanges();
  });

  it('should create team list component', () => {
    expect(component).toBeTruthy();
  });

  it('should getTeamData() be called once', () => {
    const getTeamSpy = spyOn(component, 'getTeamData');
    component.ngOnInit();
    expect(getTeamSpy).toHaveBeenCalledTimes(1);
  });

  it('should have a text if team list is empty', () => {
    spyOn(component, 'getTeamData').and.returnValue(of());
    component.ngOnInit();
    const emptyListParagraph: HTMLParagraphElement = fixture.debugElement.nativeElement.querySelector(
      'p'
    );
    component.team$.subscribe((_) => {
      fixture.detectChanges();
      return expect(emptyListParagraph.textContent).toEqual(
        'Sorry team list is empty'
      );
    });
  });

  it('should list teammates not to be empty', () => {
    const team: Team = {
      title: 'Our team',
      teammates: [
        {
          imageUrl: { w200: 'https://images.unsplash.com/photo' },
          block: {
            title: 'Sample title',
            description: 'Sample description',
            link: 'user@fake.com',
            text: '123445556',
          },
        },
      ],
    };
    spyOn(component, 'getTeamData').and.returnValue(of(team));
    component.ngOnInit();
    component.team$.subscribe((teamResponse) => {
      fixture.detectChanges();
      const teammateCard = fixture.debugElement.queryAll(
        By.css('.teammate-card')
      );
      expect(teamResponse.teammates.length).toBe(team.teammates.length);
      expect(teammateCard[0].query(By.css('img')).nativeElement.src).toBe(
        team.teammates[0].imageUrl.w200
      );
      expect(
        getFirstNativeElement(teammateCard, 'mat-card-title').textContent.trim()
      ).toBe(team.teammates[0].block.title);
      expect(
        getFirstNativeElement(
          teammateCard,
          'mat-card-subtitle'
        ).textContent.trim()
      ).toBe(team.teammates[0].block.description);
      expect(
        getFirstNativeElement(
          teammateCard,
          '.contact-info a'
        ).textContent.trim()
      ).toBe(team.teammates[0].block.link);
      expect(
        getFirstNativeElement(teammateCard, '.phone-number').textContent.trim()
      ).toBe(team.teammates[0].block.text);
    });
  });
});
