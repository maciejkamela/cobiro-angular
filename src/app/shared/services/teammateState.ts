import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ServiceNameService {
  teammateCounter: BehaviorSubject<number> = new BehaviorSubject(0);
  teammateCounter$: Observable<number> = this.teammateCounter.asObservable();

  constructor() {}

  increaseCounter() {
    return this.teammateCounter.next(this.teammateCounter.value + 1);
  }
}
