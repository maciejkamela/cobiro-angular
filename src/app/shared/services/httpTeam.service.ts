import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Team } from '../models/team.model';
import { APP_URL } from '../../app.config';
import { GetTeam } from '../models/team-service.model';
import { pluck, map } from 'rxjs/operators';

@Injectable()
export class HttpUserService implements GetTeam {
  constructor(@Inject(APP_URL) private url: string, private http: HttpClient) {}

  getTeamData(): Observable<Team> {
    return this.http.get(`${this.url}/task/index.json`).pipe(
      pluck('data'),
      map((response) => {
        const team: Team = {
          title: response[0].attributes.title,
          teammates: Object.values(response[0].attributes.memberCards),
        };
        return team;
      })
    );
  }


}
