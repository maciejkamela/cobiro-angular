import { Team } from './team.model';
import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';

export interface GetTeam {
  getTeamData(): Observable<Team>;
}

export const GET_TEAM = new InjectionToken<Team>('GET_TEAM');
