import { Observable } from 'rxjs';
import { Team } from './team.model';

export abstract class AbstractUserService {
  abstract getTeamData(): Observable<Team[]>;
}
