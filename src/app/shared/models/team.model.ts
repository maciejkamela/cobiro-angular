export interface Teammate {
  imageUrl: { [key: string]: string };
  block: {
    title: string;
    description: string;
    link: string;
    text: string;
  };
}

export interface Team {
  title: string;
  teammates: Teammate[];
}
